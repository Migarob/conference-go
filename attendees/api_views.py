from django.http import JsonResponse
from .models import Attendee
from events.models import Conference
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
    ]

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = [
            {
                "name": t.name,
                "href": t.get_api_url(),
            }
            for t in Attendee.objects.filter(conference=conference_id)
        ]
        return JsonResponse({"attendees": attendees})
    else:
        content = json.loads(request.body)
    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )
    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )

def api_show_attendee(request, id):
    attendee = Attendee.objects.get(id=id)
    return JsonResponse({
        "email": attendee.email,
        "name": attendee.name,
        "company_name": attendee.company_name,
        "created": attendee.created,
        "conference": {
            "name": attendee.conference.name,
            "href": attendee.conference.get_api_url(),
        }
    })
